import { Writable, writable } from "svelte/store";
import type { TokenProps } from "../constants/services";

const user: Writable<TokenProps> = writable({});

const login = (username: string) =>
  user.set({
    user: {
      username,
    },
    token: "this is a token!",
  });

const logout = () => user.set({});

export const userEvents = { login, logout };
export const userEffects = {};
export const userStores = { user };
